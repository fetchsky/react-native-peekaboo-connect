require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name           = "RNPeekabooConnect"
  s.version        = package['version']
  s.summary        = package['summary']
  s.description    = package['description']
  s.license        = package['license']
  s.author         = package['author']
  s.homepage       = package['homepage']
  s.source         = { :git => 'https://gitlab.com/fetchsky/react-native-peekaboo-connect', :tag => s.version }

  s.requires_arc   = true
  s.platform       = :ios, '8.0'

  s.source_files   = "ios/*.{h,m}"
  s.resource       = "ios/assets/PeekabooConnect.bundle"

  s.dependency 'React'
end

  