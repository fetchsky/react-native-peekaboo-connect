import { NativeModules } from 'react-native';

const { RNPeekabooConnect } = NativeModules;

export default RNPeekabooConnect;
