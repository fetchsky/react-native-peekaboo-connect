
package com.fetchsky.RNPeekabooConnect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;

import static androidx.core.app.ActivityCompat.startActivityForResult;

public class RNPeekabooConnectModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  public static int APP_REQUEST_CODE = 72;
  private Promise pendingPromise;

  private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
      Log.i("PeekabooConnect", "Activity Result");
      if (requestCode == APP_REQUEST_CODE) {
        if (resultCode == Activity.RESULT_OK) {
          WritableMap map = Arguments.createMap();
          map.putBoolean("success", true);
          if (data != null) {
            try {
              map.putString("type", data.getStringExtra("type"));
            } catch (Exception e) {
              e.printStackTrace();
              Log.e("EM", e.getMessage());
            }
          }
          resolvePromise(map);
        } else {
          rejectPromise("error", new Error("Peekaboo Connect failed"));
        }
      }
    }
  };

  public RNPeekabooConnectModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    reactContext.addActivityEventListener(mActivityEventListener);
  }


  private void rejectPromiseOnException(Exception e) {
    e.printStackTrace();
    rejectPromise("error", new Error(e.getMessage()));
  }

  private void rejectPromise(String code, Error err) {
    if (this.pendingPromise != null) {
      this.pendingPromise.reject(code, err);
      this.pendingPromise = null;
    }
  }

  private void resolvePromise(WritableMap data) {
    Log.i("PeekabooConnect", "Resolving");
    if (this.pendingPromise != null) {
      this.pendingPromise.resolve(data);
      this.pendingPromise = null;
    }
  }

  private void resolvePromise() {
    if (this.pendingPromise != null) {
      Log.i("PeekabooConnect", "Resolving");
      WritableMap map = Arguments.createMap();
      map.putBoolean("success", true);
      this.pendingPromise.resolve(map);
      this.pendingPromise = null;
    }
  }

  private static Bundle getBundleFromMap(ReadableMap readableMap) {
    ReadableMapKeySetIterator iterator = readableMap.keySetIterator();
    Bundle bundle = new Bundle();

    while (iterator.hasNextKey()) {
      String key = iterator.nextKey();
      ReadableType type = readableMap.getType(key);
      switch (type) {
        case Null:
          bundle.putInt(key, -1);
          break;
        case Boolean:
          bundle.putBoolean(key, readableMap.getBoolean(key));
          break;
        case Number:
          bundle.putDouble(key, readableMap.getDouble(key));
          break;
        case String:
          bundle.putString(key, readableMap.getString(key));
          break;
        case Map:
          bundle.putBundle(key, getBundleFromMap(readableMap.getMap(key)));
          break;
      }
    }
    return bundle;
  }

  @ReactMethod
  public void startPeekabooConnect(ReadableMap initialProps, Promise promise) {
    this.pendingPromise = promise;
    final Intent intent = new Intent(getCurrentActivity(), PeekabooConnectActivity.class);
    intent.putExtras(getBundleFromMap(initialProps));
    this.reactContext.startActivityForResult(intent, APP_REQUEST_CODE, new Bundle());
  }

  @Override
  public String getName() {
    return "RNPeekabooConnect";
  }
}