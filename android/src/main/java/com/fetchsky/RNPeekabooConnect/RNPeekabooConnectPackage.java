
package com.fetchsky.RNPeekabooConnect;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;
import com.fetchsky.RNPeekabooConnect.RNPeekabooConnectModule;

public class RNPeekabooConnectPackage implements ReactPackage {
    static List<ReactPackage> appPackages;

    public  RNPeekabooConnectPackage(List<ReactPackage> packages) {
        appPackages = packages;
    }

    public static List<ReactPackage> getAppPackages() {
        return appPackages;
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
      return Arrays.<NativeModule>asList(new RNPeekabooConnectModule(reactContext));
    }

    // Deprecated from RN 0.47
    public List<Class<? extends JavaScriptModule>> createJSModules() {
      return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
      return Collections.emptyList();
    }
}