# react-native-peekaboo-connect

## Getting started

`$ npm install https://gitlab.com/fetchsky/react-native-peekaboo-connect --save`

### Manual installation

#### iOS

To be populated soon

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`

- Add `import com.fetchsky.RNPeekabooConnectPackage;` to the imports at the top of the file
- Add `new RNPeekabooConnectPackage(packages)` to the list returned by the `getPackages()` method, it will look like:

```java
        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
          // packages.add(new MyReactNativePackage());
          packages.add(new RNPeekabooConnectPackage(packages));
          return packages;
        }
```

2. Append the following lines to `android/settings.gradle`:

```groovy
include ':react-native-peekaboo-connect'
project(':react-native-peekaboo-connect').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-peekaboo-connect/android')
```

3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:

```groovy
implementation project(':react-native-peekaboo-connect')
```

## Usage

```javascript
import RNPeekabooConnect from "react-native-peekaboo-connect";

RNPeekabooConnect.startPeekabooConnect({
	type: "deals", // deals, locator, carFinance, transaction, instalment, rewards
	pkbc: "", // Unique id shared with partner
	environment: "production", // production, stage, beta
	country: "Pakistan",
	// initialRoute: "/", optional
	// fontFamily: 'Roboto', optional
	// enableRedemption: true, In case when you have to allow user to redeem deals
	// userId: '', To identify user
	// associations: '', User's tier id
});
```
