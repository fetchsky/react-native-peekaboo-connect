//
//  PeekabooConnectBridge.h
//  RNPeekabooConnect
//
//  Created by Mohammad Zain on 16/08/2019.
//
#import <React/RCTBridgeDelegate.h>

@interface PeekabooConnectBridge : NSObject <RCTBridgeDelegate>

+ (id)sharedManager;

- (instancetype) init;

//- (void)setBridge: (RCTBridge *)appBridge;

- (RCTBridge *)getBridge;

@end
