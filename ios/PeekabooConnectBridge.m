
#import <React/RCTBridge.h>

#import "PeekabooConnectBridge.h"

@implementation PeekabooConnectBridge {
    RCTBridge *bridge;
}

- (instancetype) init {
    self = [super init];
    return self;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

//- (void)setBridge: (RCTBridge *)appBridge {
//    bridge = appBridge;
//}

- (RCTBridge *)getBridge {
    if (!bridge || bridge == nil) {
        bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:@{}];
    }
    return bridge;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"PeekabooConnect" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    NSURL *jsCodeLocation = [bundle URLForResource:@"peekaboo" withExtension:@"jsbundle"];
    return jsCodeLocation;
}




@end
  
