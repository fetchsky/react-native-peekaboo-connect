//
//  PeekabooConnectViewController.m
//  RNPeekabooConnect
//
//  Created by Mohammad Zain on 16/08/2019.
//

#import <Foundation/Foundation.h>
#import <CoreText/CTFontManager.h>

#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#import <React/RCTRootView.h>

#import "PeekabooConnectBridge.h"
#import "PeekabooConnectViewController.h"

@implementation PeekabooConnectViewController
{
    RCTPromiseResolveBlock _resolve;
    RCTPromiseRejectBlock _reject;
    RCTBridge *bridge;
    NSDictionary *initialProperties;
    bool initialized;
    bool isRunning;
};

static NSString *bundleTitle = @"PeekabooConnect";

-(void)loadFont:
(NSBundle *)bundle
          title: (NSString *)title;  {
    NSData *inData = [NSData dataWithContentsOfURL:[bundle URLForResource:title withExtension:@"ttf"]];
    if (!inData) {
        NSLog(@"Error loading in font %@", title);
        return;
    }
    CFErrorRef error;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)inData);
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
        CFStringRef errorDescription = CFErrorCopyDescription(error);
        NSLog(@"Error %@", errorDescription);
        CFRelease(errorDescription);
    }
    CFRelease(font);
    CFRelease(provider);
}


- (void)initializePeekaboo
{
    if (initialized) {
        return;
    }
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:bundleTitle ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    NSArray *fonts = @[@"Feather", @"SimpleLineIcons", @"Quicksand", @"Quicksand_light", @"Quicksand_medium", @"Quicksand_bold"];
    for (NSString *title in fonts) {
        [self loadFont:bundle title:title];
    }
    initialized = true;
}

- (void)start: (NSDictionary *)initialProps
          resolver: (RCTPromiseResolveBlock)resolve
          rejecter: (RCTPromiseRejectBlock)reject;
{
    _resolve = resolve;
    _reject = reject;
    initialProperties = initialProps;
}


- (void)viewWillAppear:(BOOL)animated
{
    if (isRunning) {
        [self dismissViewControllerAnimated:YES completion:nil];
        isRunning = false;
        return;
    }
    isRunning = true;
}


-(void)viewDidLoad {
    if (!initialized) {
        [self initializePeekaboo];
    }
    RCTBridge *bridge = [PeekabooConnectBridge.sharedManager getBridge];
    RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                     moduleName:@"peekaboosdk"
                                              initialProperties:initialProperties];
    rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    self.view = rootView;
}

- (void)viewDidDisappear:(BOOL)animated {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _resolve([userDefaults valueForKey:@"pkb_exit_data"]);
    NSLog(@"viewDidDisappear");
}

- (void)viewControllerDidCancel:(UIViewController*)vc
{
}


@end
