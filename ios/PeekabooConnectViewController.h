//
//  PeekabooConnectViewController.h
//  RNPeekabooConnect
//
//  Created by Mohammad Zain on 16/08/2019.
//

#ifndef PeekabooConnectViewController_h
#define PeekabooConnectViewController_h

@interface PeekabooConnectViewController : UIViewController

//+ (id)sharedManager;
//
//- (instancetype) init;

//- (void)setBridge: (RCTBridge *)appBridge;

- (void)start: (NSDictionary*)params
              resolver: (RCTPromiseResolveBlock)resolve
              rejecter: (RCTPromiseRejectBlock)reject;
@end


#endif /* PeekabooConnectViewController_h */
