
#import "RNPeekabooConnect.h"
#import "PeekabooConnectBridge.h"
#import "PeekabooConnectViewController.h"

@implementation RNPeekabooConnect {
    PeekabooConnectViewController *vc;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()



- (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    if ([topVC isKindOfClass:[UINavigationController class]]) {
        return [(UINavigationController *)topVC topViewController];
    }
    return topVC;
}

RCT_REMAP_METHOD(startPeekabooConnect,
                 startPeekabooConnect:(NSDictionary *)initialProps
                 resolver: (RCTPromiseResolveBlock)resolve
                 rejecter: (RCTPromiseRejectBlock)reject)
{
    @try {
        vc = [PeekabooConnectViewController alloc];
        [vc start:initialProps resolver:resolve rejecter:reject];
        UIViewController *top = [self currentTopViewController];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        navigationController.navigationBar.hidden = true;
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
        [top presentViewController:navigationController animated:YES completion: nil];
//        [PeekabooConnectViewController.sharedManager start:initialProps resolver:resolve rejecter:reject];
    }
    @catch (NSException * e) {
        reject(@"PeekabooConnect Failed", @"Could not start", [self errorFromException:e]);
    }
}


- (NSError *) errorFromException: (NSException *) exception
{
    NSDictionary *exceptionInfo = @{
                                    @"name": exception.name,
                                    @"reason": exception.reason,
                                    @"callStackReturnAddresses": exception.callStackReturnAddresses,
                                    @"callStackSymbols": exception.callStackSymbols,
                                    @"userInfo": exception.userInfo
                                    };
    
    return [[NSError alloc] initWithDomain: @"RNPeekabooConnect"
                                      code: 0
                                  userInfo: exceptionInfo];
}


@end
  
